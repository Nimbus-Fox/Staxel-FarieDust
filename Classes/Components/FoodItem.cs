﻿using Plukit.Base;

namespace NimbusFox.FarieDust.Classes.Components {
    public class FoodItem {
        public Vector3D Offset { get; } = Vector3D.Zero;
        public bool Compact { get; }

        public FoodItem(Blob config) {
            if (config.Contains("offset")) {
                Offset = config.FetchBlob("offset").GetVector3D();
            }

            Compact = config.GetBool("compact", true);
        }
    }
}
