﻿using System;
using System.Linq;
using System.Reflection;
using NimbusFox.FarieDust.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.Classes {
    public static class Extensions {
        public static byte GetNext(this IFariePower dust) {
            if (dust.Level <= 0) {
                return 0;
            }

            return (byte)(dust.Level - 1);
        }

        public static bool IsEntityHoldingAnything(this Entity entity) {
            return !entity.Inventory.ActiveItem().IsNull();
        }

        internal static T GetPrivatePropertyValue<T>(this object parentObject, string field) {
            return (T)parentObject.GetType().GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivatePropertyValue(this object parentObject, string field, object value) {
            parentObject.GetType().GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivateFieldValue<T>(this object parentObject, string field) {
            return (T)parentObject.GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivateFieldValue(this object parentObject, string field, object value) {
            parentObject.GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivatePropertyValue<T>(this object parentObject, string field, Type type) {
            return (T)type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivatePropertyValue(this object parentObject, string field, object value, Type type) {
            type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivateFieldValue<T>(this object parentObject, string field, Type type) {
            return (T)type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivateFieldValue(this object parentObject, string field, object value, Type type) {
            type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        public static Item MakeItem(this Tile tile) {
            return tile.Configuration.MakeItem();
        }

        public static Item MakeItem(this TileConfiguration tile) {
            var itemBlob = BlobAllocator.Blob(true);
            itemBlob.SetString("kind", "staxel.item.Placer");
            itemBlob.SetString("tile", tile.Code);
            var item = GameContext.ItemDatabase.SpawnItemStack(itemBlob, null);
            Blob.Deallocate(ref itemBlob);

            if (item.IsNull()) {
                return Item.NullItem;
            }

            return item.Item;
        }

        public static Item MakeItem(string code) {
            var tile = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == code);

            if (tile != default(TileConfiguration)) {
                return tile.MakeItem();
            }

            var itemBlob = BlobAllocator.Blob(true);
            itemBlob.SetString("code", code);

            var item = GameContext.ItemDatabase.SpawnItemStack(itemBlob, null);
            Blob.Deallocate(ref itemBlob);

            if (item.IsNull()) {
                return Item.NullItem;
            }

            return item.Item;
        }
    }
}
