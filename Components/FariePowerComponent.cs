﻿using Plukit.Base;

namespace NimbusFox.FarieDust.Components {
    public class FariePowerComponent {
        public byte Channel { get; }
        public bool PassSignalThroughTile { get; }
        public string On { get; }
        public string Off { get; }

        public FariePowerComponent(Blob config) {
            if (byte.TryParse(config.GetLong("channel", 0).ToString(), out var channel)) {
                Channel = channel;
            } else {
                Channel = 0;
            }

            PassSignalThroughTile = config.GetBool("passSignalThroughTile", false);

            On = config.GetString("on", null);
            Off = config.GetString("off", null);
        }
    }
}
