﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.Components.Tiles.Builders {
    public class FarieHomeTileComponentBuilder : ITileComponentBuilder{
        public string Kind() {
            return "farieHome";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new FarieHomeTileComponent(config);
        }
    }
}
