﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.Components.Tiles.Builders {
    public class FariePowerTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "fariePower";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new FariePowerComponent(config);
        }
    }
}
