﻿using Plukit.Base;
using Staxel.Core;
using Staxel.Items;

namespace NimbusFox.FarieDust.Components.Tiles.Builders {
    class NoToolTileComponentBuilder : IComponentBuilder, IItemComponentBuilder {
        public string Kind() {
            return "noTool";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new NoToolComponent(config);
        }

        public object Instance(Blob config) {
            return new NoToolComponent(config);
        }
    }
}
