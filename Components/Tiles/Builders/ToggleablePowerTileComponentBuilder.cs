﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.Components.Tiles.Builders {
    public class ToggleablePowerTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "toggleablePower";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new ToggleablePowerTileComponent(config);
        }
    }
}
