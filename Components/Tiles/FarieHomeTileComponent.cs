﻿using System.Collections.Generic;
using NimbusFox.FarieDust.Classes.Components;
using NimbusFox.FloatingDocksAPI.Classes;
using Plukit.Base;

namespace NimbusFox.FarieDust.Components.Tiles {
    public class FarieHomeTileComponent {
        public IReadOnlyDictionary<byte, string> DustStages { get; }
        public string EmptyTile { get; }
        public string OccupiedTile { get; }
        public DockAnimation JarAnimation { get; }
        public FoodItem FoodItem { get; }
        public byte Max { get; } = 1;
        public byte Min { get; } = 3;

        public FarieHomeTileComponent(Blob config) {
            var output = new Dictionary<byte, string>();

            var stages = config.FetchBlob("dustStages");

            byte value;

            foreach (var key in stages.KeyValueIteratable.Keys) {
                var skey = key.Contains("&") ? key.Replace("%", "") : key;

                if (byte.TryParse(skey, out value)) {
                    var sValue = stages.GetString(key, null);

                    if (!sValue.IsNullOrEmpty()) {
                        output.Add(value, sValue);
                    }
                }
            }

            EmptyTile = config.GetString("empty", null);
            OccupiedTile = config.GetString("occupied", null);

            DustStages = output;

            if (config.Contains("jarAnimation")) {
                JarAnimation = new DockAnimation(config.FetchBlob("jarAnimation"));
            }

            if (config.Contains("foodItem")) {
                FoodItem = new FoodItem(config.FetchBlob("foodItem"));
            }

            if (byte.TryParse(config.GetLong("min", 1).ToString(), out value)) {
                Min = value;
            }

            if (byte.TryParse(config.GetLong("max", 3).ToString(), out value)) {
                Max = value;
            }
        }
    }
}
