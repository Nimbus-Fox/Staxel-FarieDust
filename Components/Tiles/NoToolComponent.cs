﻿using Plukit.Base;
using Staxel.Items.ItemComponents;

namespace NimbusFox.FarieDust.Components.Tiles {
    public class NoToolComponent : IToolable {

        public string Code { get; }

        public NoToolComponent(Blob config) {
            Code = config.GetString("code", null);
        }

        public string ToolCode() {
            return "nimbusfox.tool.none";
        }

        public string VerbTranslationCode() {
            return "noTool";
        }
    }
}
