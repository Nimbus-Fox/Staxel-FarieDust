﻿using Plukit.Base;

namespace NimbusFox.FarieDust.Components.Tiles {
    public class ToggleablePowerTileComponent {
        public double TimeActive { get; }
        public string ToggleOn { get; }
        public string ToggleOff { get; }
        public byte BaseLevel { get; }

        public ToggleablePowerTileComponent(Blob config) {
            ToggleOn = config.GetString("toggleOn", null);

            ToggleOff = config.GetString("toggleOff", null);

            TimeActive = config.GetDouble("timeActive", 0);

            BaseLevel = byte.TryParse(config.GetLong("baseLevel", 127).ToString(), out var value) ? value : (byte) 127;
        }
    }
}
