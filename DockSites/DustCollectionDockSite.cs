﻿using NimbusFox.FloatingDocksAPI.DockSites;
using Plukit.Base;
using Staxel.Docks;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.FarieDust.DockSites {
    public class DustCollectionDockSite : FloatingDockSite {

        public DustCollectionDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig, TileConfiguration parent) : base(entity,
            id, dockSiteConfig, parent) {
        }

        public override bool TryDock(Entity entity, EntityUniverseFacade facade, ItemStack stack, uint rotation) {
            if (entity.Logic is PlayerEntityLogic || CanDock(stack) <= 0) {
                return false;
            }

            var entry = FindEntry(stack.Item);

            if (!entry.PlaceSoundGroup.IsNullOrEmpty()) {
                BaseEffects.PlaySound(_entity, entry.PlaceSoundGroup);
            }

            if (!entry.EffectTrigger.IsNullOrEmpty()) {
                EffectQueue.Trigger(new EffectTrigger(entry.EffectTrigger));
            }

            AddToDock(entity, stack, entry, rotation);
            return true;
        }

        public new static string Name => "farieDustHolder";
    }
}
