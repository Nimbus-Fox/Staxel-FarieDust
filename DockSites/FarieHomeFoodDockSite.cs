﻿using Plukit.Base;
using Staxel.Docks;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.FarieDust.DockSites {
    public class FarieHomeFoodDockSite : DockSite {
        public FarieHomeFoodDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig) : base(entity, id, dockSiteConfig) {
        }

        public override bool TryDock(Entity entity, EntityUniverseFacade facade, ItemStack stack, uint rotation) {
            if (CanDock(stack) <= 0) {
                return false;
            }

            var entry = FindEntry(stack.Item);

            if (!entry.PlaceSoundGroup.IsNullOrEmpty()) {
                BaseEffects.PlaySound(_entity, entry.PlaceSoundGroup);
            }

            if (!entry.EffectTrigger.IsNullOrEmpty()) {
                EffectQueue.Trigger(new EffectTrigger(entry.EffectTrigger));
            }

            AddToDock(entity, stack, entry, rotation);


            
            return true;
        }
    }
}
