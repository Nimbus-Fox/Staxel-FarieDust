﻿using NimbusFox.FloatingDocksAPI.DockSites;
using Staxel.Docks;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.FarieDust.DockSites {
    public class FarieHomeJarDockSite : FloatingDockSite {
        public Item FoodItem;
        public Item JarItem;
        public FarieHomeJarDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig, TileConfiguration parent) : base(entity, id, dockSiteConfig, parent) { }

        public override bool TryDock(Entity entity, EntityUniverseFacade facade, ItemStack stack, uint rotation) {
            return false;
        }

        public override bool TryUndock(PlayerEntityLogic player, EntityUniverseFacade facade, int quantity) {
            return false;
        }
    }
}
