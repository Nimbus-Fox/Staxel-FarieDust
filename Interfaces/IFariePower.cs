﻿using System.Collections.Generic;
using Plukit.Base;

namespace NimbusFox.FarieDust.Interfaces {
    public interface IFariePower {
        byte Level { get; }
        byte Channel { get; }
        void SetLevel(byte level);
        bool PassThroughTile { get; }
        List<Vector3I> IgnoreSignalFrom { get; set; }
    }
}