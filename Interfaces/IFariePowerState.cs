﻿using Staxel.Logic;

namespace NimbusFox.FarieDust.Interfaces {
    public interface IFariePowerState {
        EntityId LogicOwner { get; }
    }
}