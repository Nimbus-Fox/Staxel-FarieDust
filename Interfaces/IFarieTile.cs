﻿using Plukit.Base;

namespace NimbusFox.FarieDust.Interfaces {
    public interface IFarieTile {
        Vector3I PedestalLocation { get; }
        bool Active { get; }
        void SetPedestalLocation(Vector3I location);
    }
}