﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.FarieDust.Items.Builders {
    public class FarieDustItemBuilder : IItemBuilder, IDisposable {

        public ItemRenderer Renderer;

        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();
        }
        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            return new FarieDust(this);
        }

        public string Kind() {
            return "nimbusfox.fariedust.item.dust";
        }
    }
}
