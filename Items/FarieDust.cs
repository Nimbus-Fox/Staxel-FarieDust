﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FarieDust.Items.Builders;
using Plukit.Base;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.Items {
    public class FarieDust : Item {

        private FarieDustItemBuilder _builder;

        public FarieDust(FarieDustItemBuilder builder) : base(builder.Kind()) {
            _builder = builder;
        }
        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }
        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        protected override void AssignFrom(Item item) { }
        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            
            return false;
        }

        public override bool HasAssociatedToolComponent(Components components) {
            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }
    }
}
