﻿using NimbusFox.FarieDust.TileEntities.Logic;
using NimbusFox.FarieDust.TileEntities.Painters;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.FarieDust.TileEntities.Builders {
    class FarieHomeTileEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public string Kind => KindCode;

        public static string KindCode => "nimusfox.fariedust.entity.tile.fariehome";

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new FarieHomeTileEntityLogic(entity);
        }

        EntityPainter IEntityPainterBuilder.Instance() {
            return new FarieHomeTilePainter();
        }

        public static Entity Spawn(Vector3I position, Blob config, EntityUniverseFacade universe) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);

            blob.SetString("kind", KindCode);
            blob.FetchBlob("position").SetVector3D(position.ToTileCenterVector3D());
            blob.FetchBlob("location").SetVector3I(position);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);
            blob.FetchBlob("config").MergeFrom(config);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            Blob.Deallocate(ref blob);

            return entity;
        }

        public void Load() {

        }

        public bool IsTileStateEntityKind() {
            return false;
        }
    }
}
