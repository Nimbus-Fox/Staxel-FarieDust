﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FarieDust.Classes.Components;
using NimbusFox.FarieDust.Components.Tiles;
using NimbusFox.FarieDust.Interfaces;
using NimbusFox.FloatingDocksAPI.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;
using Extensions = NimbusFox.FarieDust.Classes.Extensions;

namespace NimbusFox.FarieDust.TileEntities.Logic {
    public class FarieHomeTileEntityLogic : EntityLogic, IFarieTile {
        public Entity Entity { get; }
        public Vector3I Location { get; private set; }
        public TileConfiguration Configuration { get; private set; }

        private double _lastRun = 0.0;
        private Blob _constructData;
        private EntityId _parent;
        private bool _needStore = true;
        private byte _min = 1;
        private byte _max = 3;

        public Tile EmptyTile { get; private set; }
        public Tile OccupiedTile { get; private set; }
        public Item Food { get; private set; }
        public DockAnimation Animation { get; private set; }
        public FoodItem FoodPosition { get; private set; }
        public bool ShowJar = false;
        public Vector3I PedestalLocation { get; private set; } = Vector3I.Zero;
        public bool Active { get; private set; }
        public long Used { get; private set; } = 0;
        public long Total = 0;

        internal readonly Dictionary<byte, Item> DustStages = new Dictionary<byte, Item>();

        public void SetParent(EntityId id) {
            _parent = id;
        }

        public FarieHomeTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_lastRun > entityUniverseFacade.DayNightCycle().Phase ||
                _lastRun + 0.05 <= entityUniverseFacade.DayNightCycle().Phase) {
                if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {

                    var tiles = new List<Tile>();

                    for (var x = -1; x <= 1; x++) {
                        for (var z = -1; z <= 1; z++) {
                            if (entityUniverseFacade.ReadTile(new Vector3I(Location.X + x, Location.Y - 1, Location.Z + z), TileAccessFlags.SynchronousWait, out var ctile)) {
                                tiles.Add(ctile);
                            }
                        }
                    }

                    Active = tiles.All(x => x.Configuration.Code.StartsWith("staxel.tile.fairyVillage.FairyGrassLayer")) && tiles.Count == 9;

                    if (tile.Configuration == EmptyTile.Configuration) {
                        EmptyUpdate(timestep, entityUniverseFacade, tile);
                    }

                    if (tile.Configuration == OccupiedTile.Configuration) {
                        OccupiedUpdate(timestep, entityUniverseFacade, tile);
                    }

                    _lastRun = entityUniverseFacade.DayNightCycle().Phase;
                    NeedStore();
                }
            }
        }

        public void EmptyUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade, Tile tile) {
            if (ShowJar) {
                ShowJar = false;
                NeedStore();
            }
            DockSite dockSite = null;
            if (entityUniverseFacade.TryGetEntity(_parent, out var entity)) {
                foreach (var entry in entity.DockTileStateEntityLogic.GetSites()) {
                    if (entry.Config.SiteName == "foodDock") {
                        dockSite = entry;
                    }
                }
            }

            if (dockSite != null) {
                Logger.WriteLine("Here");
                if (!dockSite.IsEmpty() && Active) {
                    Logger.WriteLine("Checking if farie moves in");
                    if (GameContext.RandomSource.Next(1, 10) >= 8) {
                        if (Food == null) {
                            Food = dockSite.DockedItem.Stack.Item;
                            NeedStore();
                        }
                        entityUniverseFacade.DirectWriteTile(Location, OccupiedTile, TileAccessFlags.SynchronousWait);
                        UpdateConfig(OccupiedTile);
                        OccupiedUpdate(timestep, entityUniverseFacade, tile);
                    }
                }
            }
        }

        public void OccupiedUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade, Tile tile) {
            if (!ShowJar) {
                ShowJar = true;
                NeedStore();
            }

            if (Active) {
                if (PedestalLocation != Vector3I.Zero) {
                    var addedDust = false;
                    entityUniverseFacade.ForAllEntitiesInRange(PedestalLocation.ToVector3D(), 1f, entity => {
                        if (entity.Removed && addedDust) {
                            return;
                        }

                        if (entity.Logic is PedestalTileEntityLogic logic) {
                            if (logic.Active && logic.Location == PedestalLocation) {
                                var use = (long)GameContext.RandomSource.Next(_min, _max);


                                if (use > Total - Used) {
                                    use = Total - Used;
                                }

                                if (logic.AddDust(Entity, entityUniverseFacade, (byte)use)) {
                                    Used += use;
                                    addedDust = true;
                                }
                            }
                        }
                    });

                    if (Total == Used) {
                        Food = null;
                        entityUniverseFacade.DirectWriteTile(Location, EmptyTile, TileAccessFlags.SynchronousWait);
                        UpdateConfig(EmptyTile);
                        EmptyUpdate(timestep, entityUniverseFacade, tile);
                    }

                    if (addedDust) {
                        NeedStore();
                    }
                }
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Configuration.Code == Constants.SkyCode) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);

                    if (entityUniverseFacade.TryGetEntity(_parent, out var entity)) {
                        if (entity.TileStateEntityLogic != null) {
                            entityUniverseFacade.RemoveEntity(entity.TileStateEntityLogic.Entity.Id);
                        }
                    }
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructData = BlobAllocator.Blob(false);

            _constructData.MergeFrom(arguments);
            Location = arguments.FetchBlob("location").GetVector3I();
            var config = arguments.FetchBlob("config");
            Configuration = GameContext.TileDatabase.GetTileConfiguration(config.GetString("tile"));
            Entity.Physics.ForcedPosition(Location.ToVector3D());

            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                UpdateConfig(tile);
            }
        }

        public void UpdateConfig(Tile tile) {
            var component = tile.Configuration.Components.Select<FarieHomeTileComponent>().FirstOrDefault();

            if (component != default(FarieHomeTileComponent)) {
                EmptyTile = GameContext.TileDatabase.GetTileConfiguration(component.EmptyTile)
                    .MakeTile(tile.Variant());

                OccupiedTile = GameContext.TileDatabase.GetTileConfiguration(component.OccupiedTile)
                    .MakeTile(tile.Variant());

                DustStages.Clear();

                foreach (var sItem in component.DustStages) {
                    DustStages.Add(sItem.Key, Extensions.MakeItem(sItem.Value));
                }

                Animation = component.JarAnimation;
                FoodPosition = component.FoodItem;

                _min = component.Min;
                _max = component.Max;
            }

            if (Food == null) {
                Used = 0;
                Total = 0;
            } else {
                var foodComponent = Food.Configuration.Components.Select<PricingComponent>().FirstOrDefault();

                if (foodComponent != default(PricingComponent)) {
                    if (foodComponent.Sellable) {
                        Total = (long)Math.Floor(foodComponent.SellPrice / 10);
                    }
                }
            }
        }

        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return default;
        }

        public override bool IsPersistent() {
            return true;
        }

        public void NeedStore() {
            _needStore = true;
        }

        public override void Store() {
            if (_needStore) {
                Store(Entity.Blob);
                _needStore = false;
            }
        }

        public override void StorePersistenceData(Blob data) {
            Store(data);
            if (_constructData != null) {
                data.FetchBlob("constructData").MergeFrom(_constructData);
            }
        }

        public virtual void Store(Blob data) {
            if (Food != null) {
                data.SetString("foodCode", Food.GetItemCode());
                data.SetString("foodKind", Food.Kind);
            }

            if (_parent != EntityId.NullEntityId) {
                data.SetLong("parent", _parent.Id);
            }

            if (Configuration != null) {
                data.SetString("animationTile", Configuration.Code);
            }

            data.SetDouble("lastRun", _lastRun);

            data.SetBool("showJar", ShowJar);

            data.SetLong("used", Used);

            data.SetBool("active", Active);
        }

        public override void Restore() {
            Restore(Entity.Blob);
        }

        public virtual void Restore(Blob data) {
            if (data.Contains("foodCode") && Food == null) {
                var item = Extensions.MakeItem(data.GetString("foodCode"));

                if (item != Item.NullItem) {
                    Food = item;
                }
            }

            if (data.Contains("animationTile")) {
                var tile = GameContext.TileDatabase.GetTileConfiguration(data.GetString("animationTile")).MakeTile();
                UpdateConfig(tile);
            }

            if (data.Contains("parent")) {
                _parent = data.GetLong("parent");
            }

            if (data.Contains("lastRun")) {
                _lastRun = data.GetDouble("lastRun");
            }

            if (data.Contains("showJar")) {
                ShowJar = data.GetBool("showJar");
            }

            if (data.Contains("used")) {
                Used = data.GetLong("used");
            }

            if (data.Contains("active")) {
                Active = data.GetBool("active");
            }
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructData")) {
                Construct(data.FetchBlob("constructData"), facade);
            }

            Restore(data);
        }

        public override bool IsCollidable() {
            return false;
        }

        public void SetPedestalLocation(Vector3I location) {
            PedestalLocation = location;
        }
    }
}
