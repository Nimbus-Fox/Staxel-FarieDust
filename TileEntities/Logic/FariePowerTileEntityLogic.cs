﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FarieDust.Classes;
using NimbusFox.FarieDust.Components;
using NimbusFox.FarieDust.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.AutoTiling;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.TileEntities.Logic {
    public class FariePowerTileEntityLogic : EntityLogic, IFariePower {

        public byte Level { get; private set; }
        public byte Channel { get; private set; }
        public bool CheckIfFloating { get; set; } = true;

        public Tile OnTile { get; private set; }
        public Tile OffTile { get; private set; }

        private DateTime _lastCheck = DateTime.MinValue;

        public void SetLevel(byte level) {
            Level = level;
        }

        public bool PassThroughTile { get; private set; }
        public List<Vector3I> IgnoreSignalFrom { get; set; } = new List<Vector3I>();

        public Entity Entity { get; }
        public Vector3I Location { get; private set; }
        public TileConfiguration Configuration { get; private set; }

        public FariePowerTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public void UpdateTiling(EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                var tiles = new Vector3IMap<Tile>();
                if (TileProcessLogic.AutoTileProcess(tile.Configuration, 0, Location, Location, tile.Variant(),
                    entityUniverseFacade, TileAccessFlags.SynchronousWait, tiles)) {

                    if (tiles.Any()) {
                        var cTile = tiles.First().Value;

                        if (cTile.Configuration != tile.Configuration || tile.Variant() != cTile.Variant() ||
                            tile.Alt() != cTile.Alt()) {
                            entityUniverseFacade.DirectWriteTile(Location, cTile, TileAccessFlags.SynchronousWait);
                        }
                    } else {
                        var nTile = GameContext.TileDatabase.AllMaterials()
                            .FirstOrDefault(x => x.Code == tile.Configuration.RepresentativeTile);

                        if (nTile != default(TileConfiguration)) {
                            tile = nTile.MakeTile();
                            entityUniverseFacade.DirectWriteTile(Location, tile, TileAccessFlags.SynchronousWait);
                        }
                    }
                } else {
                    var nTile = GameContext.TileDatabase.AllMaterials()
                        .FirstOrDefault(x => x.Code == tile.Configuration.RepresentativeTile);

                    if (nTile != default(TileConfiguration)) {
                        tile = nTile.MakeTile();
                        entityUniverseFacade.DirectWriteTile(Location, tile, TileAccessFlags.SynchronousWait);
                    }
                }

            }
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (Configuration == null || Location == default) {
                return;
            }

            if (new TimeSpan(DateTime.Now.Ticks - _lastCheck.Ticks).TotalSeconds >= 0.25) {
                if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                    if (OnTile.Configuration != tile.Configuration) {
                        var component = tile.Configuration.Components.Select<FariePowerComponent>().FirstOrDefault();

                        if (component != default(FariePowerComponent)) {
                            var config = GameContext.TileDatabase.AllMaterials()
                                .FirstOrDefault(x => x.Code == component.On);

                            if (config != default(TileConfiguration)) {
                                OnTile = config.MakeTile(tile.Variant());
                            }

                            config = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == component.Off);

                            if (config != default(TileConfiguration)) {
                                OffTile = config.MakeTile(tile.Variant());
                            }
                        }
                    }
                }

                if (!Configuration.Floats && CheckIfFloating) {
                    if (entityUniverseFacade.ReadTile(new Vector3I(Location.X, Location.Y - 1, Location.Z),
                        TileAccessFlags.SynchronousWait, out var uTile)) {
                        if (uTile.Configuration.Code == "staxel.tile.Sky") {
                            if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait,
                                out var logic)) {
                                var main = new ControlState();
                                var alt = new ControlState { DownClick = true };

                                logic.Interact(Entity, entityUniverseFacade, main, alt);
                                return;
                            }
                        }
                    }
                }

                var locations = new List<Vector3I> {
                new Vector3I(Location.X + 1, Location.Y, Location.Z),
                new Vector3I(Location.X + 1, Location.Y + 1, Location.Z),
                new Vector3I(Location.X + 1, Location.Y - 1, Location.Z),
                new Vector3I(Location.X - 1, Location.Y, Location.Z),
                new Vector3I(Location.X - 1, Location.Y + 1, Location.Z),
                new Vector3I(Location.X - 1, Location.Y - 1, Location.Z),
                new Vector3I(Location.X, Location.Y, Location.Z + 1),
                new Vector3I(Location.X, Location.Y + 1, Location.Z + 1),
                new Vector3I(Location.X, Location.Y - 1, Location.Z + 1),
                new Vector3I(Location.X, Location.Y, Location.Z - 1),
                new Vector3I(Location.X, Location.Y + 1, Location.Z - 1),
                new Vector3I(Location.X, Location.Y - 1, Location.Z - 1)
            };

                SetLevel(0);

                foreach (var location in locations) {
                    if (IgnoreSignalFrom.Contains(location)) {
                        continue;
                    }
                    if (entityUniverseFacade.TryFetchTileStateEntityLogic(location, TileAccessFlags.SynchronousWait,
                        out var stateLogic)) {
                        if (stateLogic is IFariePowerState logic) {
                            if (entityUniverseFacade.TryGetEntity(logic.LogicOwner, out var entity)) {
                                if (entity.Logic is IFariePower powerLogic) {
                                    GetNextCharge(powerLogic);
                                }
                            }
                        }
                    }
                }

                if (Level == 0) {
                    if (tile != OffTile)
                    entityUniverseFacade.DirectWriteTile(Location, OffTile, TileAccessFlags.SynchronousWait);
                } else {
                    if (tile != OnTile)
                        entityUniverseFacade.DirectWriteTile(Location, OnTile, TileAccessFlags.SynchronousWait);
                }

                locations.Clear();
                _lastCheck = DateTime.Now;
            }

            IgnoreSignalFrom.Clear();
        }
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Store() {
            StorePersistenceData(Entity.Blob);
        }

        private void GetNextCharge(IFariePower logic) {
            if (Channel == 0 || Channel == logic.Channel || logic.Channel == 0) {
                var next = logic.GetNext();

                if (next > Level) {
                    SetLevel(next);
                    logic.IgnoreSignalFrom.Add(Location);
                }
            }
        }

        public override void Restore() {
            Restore(Entity.Blob);
        }

        public virtual void Restore(Blob data) {
            if (data.Contains("level")) {
                if (byte.TryParse(data.GetLong("level").ToString(), out var obyte)) {
                    Level = obyte;
                }
            }

            if (data.Contains("channel")) {
                if (byte.TryParse(data.GetLong("channel").ToString(), out var obyte)) {
                    Channel = obyte;
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.FetchBlob("location").GetVector3I();
            var config = arguments.FetchBlob("config");
            Configuration = GameContext.TileDatabase.GetTileConfiguration(config.GetString("tile"));
            Entity.Physics.ForcedPosition(Location.ToVector3D());

            if (!entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                PassThroughTile = false;
            } else {
                var component = tile.Configuration.Components.Select<FariePowerComponent>().FirstOrDefault();

                if (component == default(FariePowerComponent)) {
                    PassThroughTile = false;
                    Channel = 0;
                } else {
                    PassThroughTile = component.PassSignalThroughTile;
                    Channel = component.Channel;
                }
            }

            var entities = new Lyst<Entity>();

            entityUniverseFacade.FindAllEntitiesInRange(entities, Location.ToVector3D(), 2f, entity1 => {
                if (entity1.Removed) {
                    return false;
                }

                if (entity1.Logic is FariePowerTileEntityLogic) {
                    return true;
                }

                return false;
            });

            foreach (var ent in entities) {
                if (ent.Logic is FariePowerTileEntityLogic logic) {
                    logic.UpdateTiling(entityUniverseFacade);
                }
            }
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return true;
        }

        public override void StorePersistenceData(Blob data) {
            data.SetLong("level", Level);
            data.SetLong("channel", Channel);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            Restore(data);
        }

        public override bool IsCollidable() {
            return false;
        }
    }
}
