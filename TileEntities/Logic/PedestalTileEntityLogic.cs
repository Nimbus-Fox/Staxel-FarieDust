﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.FarieDust.Interfaces;
using NimbusFox.FarieDust.TileStates.EntityBuilders;
using NimbusFox.FarieDust.TileStates.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.TileEntities.Logic {
    public class PedestalTileEntityLogic : EntityLogic {
        public Entity Entity { get; }
        public Vector3I Location { get; private set; }
        public TileConfiguration Configuration { get; private set; }

        private bool _needStore = false;
        private Blob _constructData;
        private double _lastScan = 0.0;
        private EntityId _parent;

        public bool Active { get; private set; }

        public void SetParent(EntityId id) {
            _parent = id;
        }

        public PedestalTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_lastScan > entityUniverseFacade.DayNightCycle().Phase ||
                _lastScan + 0.05 <= entityUniverseFacade.DayNightCycle().Phase) {

                entityUniverseFacade.ForAllEntitiesInRange(Location.ToVector3D(), 4f, entity => {
                    if (entity.Removed) {
                        return;
                    }

                    if (entity.Logic is IFarieTile logic) {
                        if (Location != logic.PedestalLocation) {
                            if (logic.Active) {
                                logic.SetPedestalLocation(Location);
                            }
                        }
                    }
                });

                var tiles = new List<Tile>();

                for (var x = -1; x <= 1; x++) {
                    for (var z = -1; z <= 1; z++) {
                        if (entityUniverseFacade.ReadTile(new Vector3I(Location.X + x, Location.Y - 1, Location.Z + z), TileAccessFlags.SynchronousWait, out var ctile)) {
                            tiles.Add(ctile);
                        }
                    }
                }

                Active = tiles.All(x => x.Configuration.Code.StartsWith("staxel.tile.fairyVillage.FairyGrassLayer")) && tiles.Count == 9;

                _lastScan = entityUniverseFacade.DayNightCycle().Phase;
                NeedStore();
            }
        }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Configuration.Code == Constants.SkyCode) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);

                    if (entityUniverseFacade.TryGetEntity(_parent, out var entity)) {
                        if (entity.TileStateEntityLogic != null) {
                            entityUniverseFacade.RemoveEntity(entity.TileStateEntityLogic.Entity.Id);
                        }
                    }
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructData = BlobAllocator.Blob(true);

            _constructData.MergeFrom(arguments);

            Location = arguments.FetchBlob("location").GetVector3I();
            var config = arguments.FetchBlob("config");
            Configuration = GameContext.TileDatabase.GetTileConfiguration(config.GetString("tile"));
            Entity.Physics.ForcedPosition(Location.ToVector3D());
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return default;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override bool IsCollidable() {
            return false;
        }

        public void NeedStore() {
            _needStore = true;
        }

        public override void Store() {
            if (_needStore) {
                Store(Entity.Blob);
                _needStore = false;
            }
        }

        public override void StorePersistenceData(Blob data) {
            Store(data);
            if (_constructData != null) {
                data.FetchBlob("constructData").MergeFrom(_constructData);
            }
        }

        public virtual void Store(Blob data) {
            data.SetDouble("lastScan", _lastScan);
        }

        public override void Restore() {
            Restore(Entity.Blob);
        }

        public virtual void Restore(Blob data) {
            if (data.Contains("lastScan")) {
                _lastScan = data.GetDouble("lastScan");
            }
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructData")) {
                Construct(data.FetchBlob("constructData"), facade);
            }

            Restore(data);
        }

        public bool AddDust(Entity entity, EntityUniverseFacade facade, byte amount = 1) {
            var added = false;

            if (!facade.TryGetEntity(_parent, out _)) {
                PedestalTileStateEntityBuilder.Spawn(facade, Configuration.MakeTile(), Location);
            }

            if (facade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait, out var logic)) {
                if (logic is PedestalTileStateEntityLogic dockLogic) {
                    added = dockLogic.AddDust(entity, facade, amount);
                }
            }

            return added;
        }
    }
}
