﻿using System;
using System.Linq;
using NimbusFox.FarieDust.Components.Tiles;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.FarieDust.TileEntities.Logic {
    public class ToggleableTileEntityLogic : FariePowerTileEntityLogic {

        public Tile ToggleOn { get; set; }
        public Tile ToggleOff { get; set; }
        private byte BaseLevel { get; set; }
        private double TimeLimit { get; set; }
        private DateTime TurnedOn { get; set; }

        public ToggleableTileEntityLogic(Entity entity) : base(entity) {
            CheckIfFloating = false;
        }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (!entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                SetLevel(0);
            } else {
                if (tile.Configuration == ToggleOn.Configuration && TimeLimit > 0) {
                    if (new TimeSpan(DateTime.Now.Ticks - TurnedOn.Ticks).TotalSeconds >= TimeLimit) {
                        Toggle(entityUniverseFacade);
                    }
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            base.Construct(arguments, entityUniverseFacade);

            if (!entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                throw new Exception("Unable to readTile");
            }

            var component = Configuration.Components.Select<ToggleablePowerTileComponent>().FirstOrDefault();

            if (component == default(ToggleablePowerTileComponent)) {
                throw new Exception("Missing toggleablePower component in tile config");
            }

            var toggle = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == component.ToggleOn);

            if (toggle == default(TileConfiguration)) {
                throw new Exception("ToggleOn has an invalid tile code");
            }

            ToggleOn = toggle.MakeTile(tile.Variant());

            toggle = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == component.ToggleOff);

            if (toggle == default(TileConfiguration)) {
                throw new Exception("ToggleOff has an invalid tile code");
            }

            ToggleOff = toggle.MakeTile(tile.Variant());

            BaseLevel = component.BaseLevel;

            TimeLimit = component.TimeActive;
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            if (TimeLimit > 0) {
                facade.DirectWriteTile(Location, ToggleOff, TileAccessFlags.SynchronousWait);
                SetLevel(0);
            }
        }

        public override void Restore(Blob data) {
            base.Restore(data);

            if (data.Contains("baseLevel")) {
                if (byte.TryParse(data.GetLong("baseLevel").ToString(), out var value)) {
                    BaseLevel = value;
                }
            }
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);

            data.SetLong("baseLevel", BaseLevel);
        }

        public void Toggle(EntityUniverseFacade facade) {
            if (facade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Configuration == ToggleOn.Configuration) {
                    facade.DirectWriteTile(Location, ToggleOff, TileAccessFlags.SynchronousWait);
                    SetLevel(0);
                } else if (tile.Configuration == ToggleOff.Configuration) {
                    facade.DirectWriteTile(Location, ToggleOn, TileAccessFlags.SynchronousWait);
                    TurnedOn = DateTime.Now;
                    SetLevel(BaseLevel);
                }
            }
        }
    }
}
