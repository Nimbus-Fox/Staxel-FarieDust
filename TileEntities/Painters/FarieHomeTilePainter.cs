﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.FarieDust.TileEntities.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.FarieDust.TileEntities.Painters {
    public class FarieHomeTilePainter : EntityPainter {
        private bool _up = true;
        private float _add;
        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) { }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController, Timestep renderTimestep) {

        }

        public override void Render(DeviceContext graphics, Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (entity.Logic is FarieHomeTileEntityLogic logic) {
                if (logic.Animation != null && logic.ShowJar) {
                    var position = new Vector3D(logic.Entity.Physics.Position.X + 0.5, logic.Entity.Physics.Position.Y + 0.5, logic.Entity.Physics.Position.Z + 0.5);
                    var originalPos = new Vector3D(logic.Entity.Physics.Position.X + 0.5, logic.Entity.Physics.Position.Y + 0.5, logic.Entity.Physics.Position.Z + 0.5);

                    position.X += logic.Animation.OffSet.X;
                    position.Y += logic.Animation.OffSet.Y;
                    position.Z += logic.Animation.OffSet.Z;

                    originalPos.X += logic.Animation.OffSet.X;
                    originalPos.Y += logic.Animation.OffSet.Y;
                    originalPos.Z += logic.Animation.OffSet.Z;

                    if (position.Y + _add >= originalPos.Y + logic.Animation.Max) {
                        _up = false;
                    }

                    if (position.Y + _add <= originalPos.Y + logic.Animation.Min) {
                        _up = true;
                    }

                    _add += _up ? logic.Animation.Stage : -logic.Animation.Stage;

                    position.Y += _add;

                    var rotation = new Vector3F {
                        X = (float)(new TimeSpan(DateTime.UtcNow.Ticks).TotalMilliseconds *
                            logic.Animation.RotationSpeed % (2 * Math.PI))
                    };

                    if (logic.DustStages.Any()) {
                        var dusts = new Dictionary<byte, Item>(logic.DustStages).OrderBy(x => x.Key);
                        var jar = dusts.FirstOrDefault().Value;
                        var value = (double)(logic.Total - logic.Used) * 100 / logic.Total;
                        var percentage = Math.Round(value, 0);

                        foreach (var item in dusts) {
                            if (percentage >= item.Key) {
                                jar = item.Value;
                            }
                        }

                        var vMatrix = ItemConfiguration.DetermineDockedMatrix(
                            Matrix.CreateFromYawPitchRoll(rotation.X, rotation.Y, rotation.Z).ToMatrix4F(), matrix, renderOrigin,
                            jar, position);

                        ClientContext.ItemRendererManager.RenderInWorldFullSized(jar, graphics, vMatrix, true, false);
                    }
                }

                if (logic.Food != null && logic.FoodPosition != null) {
                    var position = new Vector3D(logic.Entity.Physics.Position.X + 0.5, logic.Entity.Physics.Position.Y + 0.5, logic.Entity.Physics.Position.Z + 0.5);

                    position.X += logic.FoodPosition.Offset.X;
                    position.Y += logic.FoodPosition.Offset.Y;
                    position.Z += logic.FoodPosition.Offset.Z;

                    var vMatrix = ItemConfiguration.DetermineDockedMatrix(
                        Matrix.CreateFromYawPitchRoll(0.0f, 0.0f, 0.0f).ToMatrix4F(), matrix, renderOrigin, logic.Food,
                        position);

                    ClientContext.ItemRendererManager.RenderInWorldFullSized(logic.Food, graphics, vMatrix, logic.FoodPosition.Compact, false);
                }
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
