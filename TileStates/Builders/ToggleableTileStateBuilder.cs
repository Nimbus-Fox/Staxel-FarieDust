﻿using System;
using NimbusFox.FarieDust.TileStates.EntityBuilders;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.FarieDust.TileStates.Builders {
    public class ToggleableTileStateBuilder : ITileStateBuilder, IDisposable {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return ToggleableTileStateEntityBuilder.Spawn(location, tile, universe);
        }

        public static string KindCode() {
            return "nimbusfox.fariedust.tileState.toggleableTile";
        }
    }
}
