﻿using NimbusFox.FarieDust.TileStates.Logic;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.FarieDust.TileStates.EntityBuilders {
    public class ToggleableTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {

        EntityPainter IEntityPainterBuilder.Instance() {
            return new BasicTileStateEntityPainter();
        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new ToggleableTileStateEntityLogic(entity);
        }

        public void Load() {

        }

        public bool IsTileStateEntityKind() {
            return true;
        }

        public string Kind => KindCode;

        public static string KindCode => "nimbusfox.fariedust.tileStateEntity.toggleableTile";

        public static Entity Spawn(Vector3I location, Tile tile, Universe facade) {
            var entity = new Entity(facade.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("tile", tile.Configuration.Code);
            blob.SetLong("variant", tile.Variant());
            blob.FetchBlob("location").SetVector3I(location);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

            entity.Construct(blob, facade);

            facade.AddEntity(entity);

            Blob.Deallocate(ref blob);

            return entity;
        }
    }
}
