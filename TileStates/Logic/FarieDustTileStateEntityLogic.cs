﻿using System.Linq;
using NimbusFox.FarieDust.Interfaces;
using NimbusFox.FarieDust.TileEntities.Builders;
using NimbusFox.FarieDust.TileEntities.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Logic;

namespace NimbusFox.FarieDust.TileStates.Logic {
    public class FarieDustTileStateEntityLogic : NoToolTileStateEntityLogic, IFariePowerState {
        private bool _despawn;

        public FarieDustTileStateEntityLogic(Entity entity) : base(entity) {
            Entity.Physics.PriorityChunkRadius(0, false);
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _despawn = arguments.GetBool("despawn", false);
            Location = arguments.FetchBlob("location").GetVector3I();
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
            if (LogicOwner == EntityId.NullEntityId) {
                var blob = BlobAllocator.Blob(true);

                blob.SetString("tile", Configuration.Code);
                blob.FetchBlob("location").SetVector3I(Location);
                blob.SetBool("ignoreSpawn", _despawn);

                var entities = new Lyst<Entity>();

                entityUniverseFacade.FindAllEntitiesInRange(entities, Location.ToVector3D(), 1F, EntityRequirements);

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    LogicOwner = tileEntity.Id;
                } else {
                    LogicOwner = SpawnEntity(blob, entityUniverseFacade).Id;
                }

                Blob.Deallocate(ref blob);
            }
        }

        public virtual bool EntityRequirements(Entity entity) {
            if (entity.Removed) {
                return false;
            }

            if (entity.Logic is FariePowerTileEntityLogic logic) {
                return Location == logic.Location;
            }

            return false;
        }

        public virtual Entity SpawnEntity(Blob config, EntityUniverseFacade entityUniverseFacade) {
            return FariePowerTileEntityBuilder.Spawn(Location, config, entityUniverseFacade);
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);

            data.SetLong("logicOwner", LogicOwner.Id);
            data.SetBool("despawn", _despawn);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            LogicOwner = data.GetLong("logicOwner", 0L);
            _despawn = data.GetBool("despawn", false);
        }
    }
}
