﻿using System.Linq;
using NimbusFox.FarieDust.DockSites;
using NimbusFox.FarieDust.TileEntities.Builders;
using NimbusFox.FarieDust.TileEntities.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.FarieDust.TileStates.Logic {
    public class FarieHomeTileStateEntityLogic : DockTileStateEntityLogic {
        private bool _despawn;
        public EntityId LogicOwner;
        public TileConfiguration Configuration;

        public FarieHomeTileStateEntityLogic(Entity entity) : base(entity) { }

        protected override void AddSite(DockSiteConfiguration config) {
            if (config.SiteName == "foodDock") {
                _dockSites.Add(new FarieHomeFoodDockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config));
            }
        }

        public override Vector3F InteractCursorColour() {
            return !HasAnyDockedItems() ? base.InteractCursorColour() : Constants.InteractCursorColour;
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            base.Construct(arguments, entityUniverseFacade);
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
        }

        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            base.Update(timestep, universe);

            if (LogicOwner == EntityId.NullEntityId) {
                var blob = BlobAllocator.Blob(true);

                blob.SetString("tile", Configuration.Code);
                blob.FetchBlob("location").SetVector3I(Location);
                blob.SetBool("ignoreSpawn", _despawn);

                var entities = new Lyst<Entity>();

                universe.FindAllEntitiesInRange(entities, Location.ToTileCenterVector3D(), 1F, EntityRequirements);

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    LogicOwner = tileEntity.Id;
                } else {
                    LogicOwner = SpawnEntity(blob, universe).Id;
                }

                if (universe.TryGetEntity(LogicOwner, out var ent)) {
                    if (ent.Logic is FarieHomeTileEntityLogic log) {
                        log.SetParent(Entity.Id);
                    }
                }

                Blob.Deallocate(ref blob);
            }
        }

        public virtual bool EntityRequirements(Entity entity) {
            if (entity.Removed) {
                return false;
            }

            if (entity.Logic is FarieHomeTileEntityLogic logic) {
                return Location == logic.Location;
            }

            return false;
        }

        public virtual Entity SpawnEntity(Blob config, EntityUniverseFacade entityUniverseFacade) {
            return FarieHomeTileEntityBuilder.Spawn(Location, config, entityUniverseFacade);
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);

            data.SetLong("logicOwner", LogicOwner.Id);
            data.SetBool("despawn", _despawn);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            LogicOwner = data.GetLong("logicOwner", 0L);
            _despawn = data.GetBool("despawn", false);
        }
    }
}
