﻿using System.Linq;
using NimbusFox.FarieDust.Components.Tiles;
using NimbusFox.FarieDust.TileEntities.Logic;
using Plukit.Base;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.FarieDust.TileStates.Logic {
    public class NoToolTileStateEntityLogic : TileStateEntityLogic {
        public EntityId LogicOwner { get; internal set; } = EntityId.NullEntityId;
        public TileConfiguration Configuration;

        private Entity _entity;

        public NoToolTileStateEntityLogic(Entity entity) : base(entity) {
            _entity = entity;
        }
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) { }
        public override void Bind() { }
        public override bool Interactable() {
            return true;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (alt.DownClick) {
                if (facade.CanRemoveTile(entity, Location, TileAccessFlags.SynchronousWait)) {
                    var code = Configuration.Code;

                    var component = Configuration.Components.Select<NoToolComponent>().FirstOrDefault();

                    if (component != default(NoToolComponent)) {
                        if (!component.Code.IsNullOrEmpty()) {
                            code = component.Code;
                        }
                    }

                    var item = new ItemStack(Classes.Extensions.MakeItem(code), 1);
                    facade.RemoveTile(entity, Location, TileAccessFlags.SynchronousWait);

                    if (LogicOwner != EntityId.NullEntityId) {
                        facade.RemoveEntity(LogicOwner);
                    }

                    facade.RemoveEntity(_entity.Id);

                    ItemEntityBuilder.SpawnDroppedItem(entity, facade, item, Location.ToTileCenterVector3D(), new Vector3D(0, 0.5, 0), Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);

                    var entities = new Lyst<Entity>();

                    facade.FindAllEntitiesInRange(entities, Location.ToVector3D(), 2f, entity1 => {
                        if (entity1.Removed) {
                            return false;
                        }

                        if (entity1.Logic is FariePowerTileEntityLogic) {
                            return true;
                        }

                        return false;
                    });

                    foreach (var ent in entities) {
                        if (ent.Logic is FariePowerTileEntityLogic logic) {
                            logic.UpdateTiling(facade);
                        } 
                    }
                }

            }
        }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return false;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }
        public override void BeingLookedAt(Entity entity) { }
        public override bool IsBeingLookedAt() {
            return true;
        }

        public override string AltInteractVerb() {
            return "nimbusfox.fariedust.verb.pickUp";
        }
    }
}
