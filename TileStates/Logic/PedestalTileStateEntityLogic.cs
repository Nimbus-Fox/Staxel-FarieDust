﻿using System;
using System.Linq;
using NimbusFox.FarieDust.DockSites;
using NimbusFox.FarieDust.TileEntities.Builders;
using NimbusFox.FarieDust.TileEntities.Logic;
using NimbusFox.FloatingDocksAPI.TileStates.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.FarieDust.TileStates.Logic {
    public class PedestalTileStateEntityLogic : FloatingDockTileStateEntityLogic {
        public EntityId LogicOwner;
        public TileConfiguration Configuration;
        public PedestalTileStateEntityLogic(Entity entity) : base(entity) { }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            base.Construct(arguments, entityUniverseFacade);
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
        }

        public override Vector3F InteractCursorColour() {
            return !HasAnyDockedItems() ? base.InteractCursorColour() : Constants.InteractCursorColour;
        }

        protected override void AddSite(DockSiteConfiguration config) {
            if (config.SiteName.StartsWith(DustCollectionDockSite.Name, StringComparison.CurrentCultureIgnoreCase)) {
                _dockSites.Add(new DustCollectionDockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config, GetTileConfig()));
                return;
            }

            base.AddSite(config);
        }

        public bool AddDust(Entity entity, EntityUniverseFacade facade, byte amount = 1) {
            var added = false;
            _dockSites.ForEach(x => {
                if (x == null || added) {
                    return;
                }

                if (x is DustCollectionDockSite site) {
                    if (site.TryDock(entity, facade,
                        new ItemStack(Classes.Extensions.MakeItem("nimbusfox.fariedust.dust.pile"), amount), 0)) {
                        added = true;
                    }
                }
            });

            return added;
        }

        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            base.Update(timestep, universe);

            if (LogicOwner == EntityId.NullEntityId) {

                var blob = BlobAllocator.Blob(true);

                blob.SetString("tile", Configuration.Code);
                blob.FetchBlob("location").SetVector3I(Location);

                var entities = new Lyst<Entity>();

                universe.FindAllEntitiesInRange(entities, Location.ToTileCenterVector3D(), 1F, EntityRequirements);

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    LogicOwner = tileEntity.Id;
                } else {
                    LogicOwner = SpawnEntity(blob, universe).Id;
                }

                if (universe.TryGetEntity(LogicOwner, out var ent)) {
                    if (ent.Logic is PedestalTileEntityLogic log) {
                        log.SetParent(Entity.Id);
                    }
                }

                Blob.Deallocate(ref blob);
            }
        }

        public virtual bool EntityRequirements(Entity entity) {
            if (entity.Removed) {
                return false;
            }

            if (entity.Logic is PedestalTileEntityLogic logic) {
                return Location == logic.Location;
            }

            return false;
        }

        public virtual Entity SpawnEntity(Blob config, EntityUniverseFacade entityUniverseFacade) {
            return PedestalTileEntityBuilder.Spawn(Location, config, entityUniverseFacade);
        }
    }
}
