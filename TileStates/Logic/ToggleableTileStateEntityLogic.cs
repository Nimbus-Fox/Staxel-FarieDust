﻿using NimbusFox.FarieDust.TileEntities.Builders;
using NimbusFox.FarieDust.TileEntities.Logic;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.FarieDust.TileStates.Logic {
    public class ToggleableTileStateEntityLogic : FarieDustTileStateEntityLogic {
        public ToggleableTileStateEntityLogic(Entity entity) : base(entity) {

        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (alt.DownClick) {
                if (facade.TryGetEntity(LogicOwner, out var tEntity)) {
                    if (tEntity.Logic is ToggleableTileEntityLogic logic) {
                        logic.Toggle(facade);
                    }
                }
            }
        }

        public override string AltInteractVerb() {
            return Configuration.InteractVerb;
        }

        public override bool EntityRequirements(Entity entity) {
            if (entity.Removed) {
                return false;
            }

            if (entity.Logic is ToggleableTileEntityLogic logic) {
                return Location == logic.Location;
            }

            return false;
        }

        public override Entity SpawnEntity(Blob config, EntityUniverseFacade entityUniverseFacade) {
            return ToggleableTileEntityBuilder.Spawn(Location, config, entityUniverseFacade);
        }
    }
}
